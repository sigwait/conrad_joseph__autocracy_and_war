book.name := conrad,joseph__autocracy_and_war
src := .

book.id := $(shell echo "$(book.name)" | sha1sum | head -c 7)
out := _out
book := $(out)/$(book.id)
book.epub := $(out)/$(book.name).epub
book.mobi := $(out)/$(book.name).mobi

all: res

static.dest := $(patsubst $(src)/%, $(book)/%, \
	$(wildcard $(src)/*.xhtml $(src)/*.css) $(src)/META-INF/container.xml)
$(static.dest): $(book)/%: %; $(copy)
res := $(static.dest)

$(book)/%: %.erb
	$(mkdir)
	erb $< > $@
$(book)/colophon.xhtml: $(book)/content.opf $(src)/colophon.xhtml.erb
	erb content_opf=$< $(src)/colophon.xhtml.erb > $@
res += $(patsubst $(src)/%.erb, $(book)/%, $(wildcard $(src)/*.erb))

$(book)/cover.png: $(src)/cover.svg; inkscape $< -w 1600 -h 2560 -o $@
res += $(book)/cover.png

$(book.epub): $(res)
	zip -X0 -j $@.tmp $(src)/mimetype
	cd $(book) && zip -r "$(CURDIR)/$@.tmp" *
	epub-hyphen -i table $@.tmp -o $@
	@rm $@.tmp

res: $(res)
mobi: $(book.mobi)
%.mobi: %.epub; kindlegen $<

epub: $(book.epub)
mobi: $(book.mobi)
lint: $(book.epub); epubcheck $<



mkdir = @mkdir -p $(dir $@)
define copy =
$(mkdir)
cp $< $@
endef
.DELETE_ON_ERROR:
